from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from .router import urlpatterns as urls

admin.site.site_header = "FFBC Sports"

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(urls)),
    path("payment/", include("apps.payments.urls")),
    path("api/docs/", include("docs.urls")),
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path("api/__debug__/", include(debug_toolbar.urls))] + urlpatterns
