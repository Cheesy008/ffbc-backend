from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenRefreshView

from apps.catalogs.views import GalleryViewSet, SponsorViewSet
from apps.competitions.views import CompetitionViewSet
from apps.core.views import health_check
from apps.orders.views import (
    OrderViewSet,
    OrderMeasureViewSet,
    OrderAdditionalPhotoViewSet,
    OrderItemViewSet,
    CartViewSet,
    CartItemViewSet,
    OrderContactServiceViewSet,
)
from apps.products.views import (
    ProductViewSet,
    ProductFeatureViewSet,
    MeasureViewSet,
    ProductTypeViewSet,
)
from apps.users.views import (
    AuthViewSet,
    UserViewset,
    ContactServiceViewSet,
    UserContactServiceViewSet,
    UserMeasureViewSet,
)

router = DefaultRouter()

# Auth
router.register(r"auth", AuthViewSet, "auth")

# User
router.register(r"user", UserViewset, "user")
router.register(r"contact-services", ContactServiceViewSet, "contact_services")
router.register(r"user-contact-services", UserContactServiceViewSet, "user_contact_services")
router.register(r"user-measures", UserMeasureViewSet, "user_measures")

# Catalogs
router.register(r"gallery", GalleryViewSet, "gallery")
router.register(r"sponsor", SponsorViewSet, "sponsor")

# Competitions
router.register(r"competition", CompetitionViewSet, "competition")

# Products
router.register(r"products", ProductViewSet, "products")
router.register(r"product-features", ProductFeatureViewSet, "product_features")
router.register(r"product-types", ProductTypeViewSet, "product_types")
router.register(r"measures", MeasureViewSet, "measures")

# Order
router.register(r"orders", OrderViewSet, "orders")
router.register(r"order-items", OrderItemViewSet, "order_items")
router.register(r"order-photos", OrderAdditionalPhotoViewSet, "order_photos")
router.register(r"order-measures", OrderMeasureViewSet, "order_measures")
router.register(r"order-contact-services", OrderContactServiceViewSet, "order_contact_services")

# Cart
router.register(r"cart", CartViewSet, "cart")
router.register(r"cart-items", CartItemViewSet, "cart_items")


urlpatterns = [
    # Health check
    path("health-check/", health_check, name="health-check"),
    # Auth
    path("auth/login/refresh/", TokenRefreshView.as_view(), name="login_refresh"),
]

urlpatterns += router.urls
