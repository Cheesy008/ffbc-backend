# Makefile для поднятия кластера в локальном окружении

K8S_NAMESPACE = ffbc
HELM_ENV = develop

# Django settings
DEBUG=true
SECRET_KEY=safsafsafsafdsaf
SENTRY_DSN=https://5dac54bca4b549688fd063883dffdf14@o417485.ingest.sentry.io/6187995

# Email
EMAIL=mr.world008@gmail.com
EMAIL_PASS=yhqikwqligiizyti

# Database
POSTGRES_DB=fitness_db
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST=postgres
POSTGRES_PORT=5432

# Redis
REDIS_HOST=redis
REDIS_PORT=6379

# AWS S3 settings
USE_S3=FALSE

# Nginx settings
CERTBOT_EMAIL=mr.world008@gmail.com
ENVSUBST_VARS=DOMAIN
DOMAIN=rainbow-siege-developers.ru

# Registry
REGISTRY_BACKEND=registry.gitlab.com/cheesy008/ffbc-backend/backend
REGISTRY_NGINX=registry.gitlab.com/cheesy008/ffbc-backend/nginx


build_ingress:
	helm upgrade ingress k8s/ingress --debug --install --create-namespace \
        --namespace=$(K8S_NAMESPACE)

#build_nginx:
#	helm install nginx devops/helm/nginx --create-namespace \
#        --namespace=$(K8S_NAMESPACE) \
#        --set global.env=$(HELM_ENV) \
#        --set nginx.image=registry.gitlab.idacloud.ru/idaPROJECT/absolute-realty/premium/nginx:latest \
#        --set nginx.namespace=$(K8S_NAMESPACE) \
#        --set static.pvName=s3-nginx-static-$(PROJECT)-$(HELM_ENV) \
#        --set static.pvcName=s3-nginx-static \
#        --set static.pvcS3Path=backend-content-$(PROJECT)/static

build_backend:
	helm upgrade backend k8s/backend --debug --install --create-namespace \
        --namespace=$(K8S_NAMESPACE) \
        --set global.env=$(HELM_ENV) \
        --set secrets.secretKey=$(SECRET_KEY) \
        --set secrets.debug=$(DEBUG) \
        --set secrets.email=$(EMAIL) \
        --set secrets.emailPass=$(EMAIL_PASS) \
        --set secrets.postgresDb=$(POSTGRES_DB) \
        --set secrets.postgresUser=$(POSTGRES_USER) \
        --set secrets.postgresPassword=$(POSTGRES_PASSWORD) \
        --set secrets.postgresPort=$(POSTGRES_PORT) \
        --set secrets.redisPort=$(REDIS_PORT) \
        --set secrets.useS3=$(USE_S3) \
        --set secrets.sentryDsn=$(SENTRY_DSN) \

build_celery:
	helm upgrade celery k8s/celery --debug --install --create-namespace \
        --namespace=$(K8S_NAMESPACE) \
        --set global.env=$(HELM_ENV) \
        --set secrets.secretKey=$(SECRET_KEY) \
        --set secrets.debug=$(DEBUG) \
        --set secrets.email=$(EMAIL) \
        --set secrets.emailPass=$(EMAIL_PASS) \
        --set secrets.postgresDb=$(POSTGRES_DB) \
        --set secrets.postgresUser=$(POSTGRES_USER) \
        --set secrets.postgresPassword=$(POSTGRES_PASSWORD) \
        --set secrets.postgresPort=$(POSTGRES_PORT) \
        --set secrets.redisPort=$(REDIS_PORT) \
        --set secrets.useS3=$(USE_S3) \
        --set secrets.sentryDsn=$(SENTRY_DSN) \


build_postgres:
	helm upgrade postgresql k8s/postgresql --debug --install --create-namespace \
        --namespace=$(K8S_NAMESPACE) \
        --set global.env=$(HELM_ENV) \
        --set postgresql.postgresDb=$(POSTGRES_DB) \
        --set postgresql.postgresUser=$(POSTGRES_USER) \
        --set postgresql.postgresPassword=$(POSTGRES_PASSWORD) \

build_redis:
	helm upgrade redis k8s/redis --debug --install --create-namespace \
        --namespace=$(K8S_NAMESPACE) \
        --set global.env=$(HELM_ENV)
