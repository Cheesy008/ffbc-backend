from django.contrib import admin

from .models import GalleryCategory, GalleryImage, Sponsor


@admin.register(GalleryCategory)
class GalleryCategoryAdmin(admin.ModelAdmin):
    class GalleryImageTabularInline(admin.TabularInline):
        model = GalleryImage
        extra = 0

    inlines = [GalleryImageTabularInline]


admin.site.register(Sponsor)
