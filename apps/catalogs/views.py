from rest_framework import viewsets

from .models import GalleryCategory, Sponsor
from .serializers import (
    GalleryCategoryListSerializer,
    GalleryCategoryDetailSerializer,
    SponsorSerializer,
)


class GalleryViewSet(viewsets.ModelViewSet):
    """
    Получение категорий и изображений.
    """

    queryset = GalleryCategory.objects.prefetch_related("images").all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return GalleryCategoryDetailSerializer
        return GalleryCategoryListSerializer


class SponsorViewSet(viewsets.ModelViewSet):
    """
    Получение спонсоров.
    """

    queryset = Sponsor.objects.all()
    serializer_class = SponsorSerializer
