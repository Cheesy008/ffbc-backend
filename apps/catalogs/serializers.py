from rest_framework import serializers

from .models import GalleryCategory, GalleryImage, Sponsor


# region Gallery
class GalleryImageSerialier(serializers.ModelSerializer):
    """
    Сериалайзер для вывода изображений из галереи.
    """

    class Meta:
        model = GalleryImage
        fields = (
            "id",
            "image",
        )
        read_only_fields = ("__all__",)


class GalleryCategoryListSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода списка категорий.
    """

    class Meta:
        model = GalleryCategory
        fields = (
            "id",
            "name",
        )
        read_only_fields = ("__all__",)


class GalleryCategoryDetailSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода конкретной категории вместе с изображениями.
    """

    images = GalleryImageSerialier(many=True)

    class Meta:
        model = GalleryCategory
        fields = (
            "id",
            "name",
            "images",
        )
        read_only_fields = ("__all__",)


# endregion


class SponsorSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода спонсоров.
    """

    class Meta:
        model = Sponsor
        fields = ("id", "name", "logo", "description")
        read_only_fields = ("__all__",)
