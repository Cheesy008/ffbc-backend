from django.db import models
from django.utils.translation import ugettext_lazy as _

# region Gallery
from apps.core.validators import validate_file_extension


class GalleryImage(models.Model):
    """
    Модель изображения из галереи.
    """

    image = models.FileField(
        "Gallery image",
        upload_to="catalogs/gallery/",
        null=True,
        blank=True,
        validators=[validate_file_extension],
    )
    gallery_category = models.ForeignKey(
        "GalleryCategory",
        on_delete=models.CASCADE,
        related_name="images",
        verbose_name="Category",
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("Gallery image")
        verbose_name_plural = _("Gallery images")

    def __str__(self) -> str:
        return f"Gallery image #{self.id}"


class GalleryCategory(models.Model):
    """
    Модель категории из галереи.
    """

    name = models.CharField(_("Category name"), max_length=150, null=True, blank=True)

    class Meta:
        verbose_name = _("Gallery category")
        verbose_name_plural = _("Gallery categories")
        ordering = ["name"]

    def __str__(self) -> str:
        return self.name


# endregion


class Sponsor(models.Model):
    """
    Модель спонсора.
    """

    name = models.CharField(_("Brand name"), max_length=150, null=True, blank=True)
    logo = models.FileField(
        "Logo",
        upload_to="catalogs/brand/",
        null=True,
        blank=True,
    )
    description = models.TextField(_("Description"), null=True, blank=True)

    class Meta:
        verbose_name = _("Sponsor")
        verbose_name_plural = _("Sponsors")
        ordering = ["name"]
