import json

from django.http import JsonResponse
from django.views import View

from apps.payments.services import process_order


class PaypalWebhookView(View):
    def post(self, request, *args, **kwargs):
        webhook_event = json.loads(request.body)
        process_order(webhook_event)
        return JsonResponse(data={"ok": True})
