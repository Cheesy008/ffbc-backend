from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from .views import PaypalWebhookView

urlpatterns = [
    path("paypal-webhook/", csrf_exempt(PaypalWebhookView.as_view())),
]
