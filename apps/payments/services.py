from apps.core.enums import OrderStatusType
from apps.orders.models import Order


def process_order(webhook_event):
    purchase_units = webhook_event["resource"]["purchase_units"]
    for item in purchase_units:
        if "custom_id" in item and item["custom_id"]:
            order_id = item["custom_id"]
            break
    else:
        raise Exception("Получены некорректные данные заказа")
    order = Order.objects.get(id=int(order_id))
    order.order_status = OrderStatusType.IN_THE_WORK
    order.save()
