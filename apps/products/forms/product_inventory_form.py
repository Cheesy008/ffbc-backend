# from django.db.models import Sum, Count
# from django.forms import ModelForm, ValidationError
#
# from ..models import ProductInventory
#
#
# class ProductInventoryForm(ModelForm):
#     class Meta:
#         model = ProductInventory
#         fields = "__all__"
#
#     def clean(self):
#         cleaned_data = super().clean()
#         product_feature_fields = cleaned_data.get("product_feature_fields")
#         product = cleaned_data.get("product")
#
#         if product_feature_fields and product:
#             count_feature_fields = product_feature_fields.values(
#                 "feature", "feature__name"
#             ).annotate(count=Count("feature"))
#
#             for item in count_feature_fields:
#                 if item["count"] >= 2:
#                     raise ValidationError(
#                         "The product inventory must have only one feature field on each feature"
#                     )
#
#             feature_fields = product_feature_fields.values_list("id", "name")
#             # for id, name in feature_fields:
#             #     if not product.product_features.filter(feature_fields__id=id).exists():
#             #         raise ValidationError(
#             #             f"Feature field '{name}' is not present in product features"
#             #         )
#
#     def save(self, commit=True):
#         instance: ProductInventory = super().save(commit)
#         # Расчет цены товара
#         total_feature_price = (
#             instance.product_feature_fields.aggregate(sum=Sum("feature_price")).get("sum") or 0
#         )
#         instance.final_price = instance.product.base_price + total_feature_price
#         instance.save()
#         return instance
