from rest_framework import viewsets

from ..models import MeasureCategory
from ..serializers import MeasureCategorySerializer


class MeasureViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MeasureCategory.objects.all()
    serializer_class = MeasureCategorySerializer
    pagination_class = None
