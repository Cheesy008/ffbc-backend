from rest_framework import viewsets

from ..models import ProductFeature
from ..serializers import ProductFeatureSerializer


class ProductFeatureViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ProductFeature.objects.all()
    serializer_class = ProductFeatureSerializer
    pagination_class = None
