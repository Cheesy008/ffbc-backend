from rest_framework import viewsets

from ..models import ProductType
from ..serializers import ProductTypeSerializer


class ProductTypeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ProductType.objects.all()
    serializer_class = ProductTypeSerializer
    pagination_class = None
