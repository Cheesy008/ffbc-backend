from rest_framework import serializers

from ..models import ProductFeature, ProductFeatureField


class ProductFeatureFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductFeatureField
        fields = "__all__"


class ProductFeatureSerializer(serializers.ModelSerializer):
    feature_fields = ProductFeatureFieldSerializer(many=True)

    class Meta:
        model = ProductFeature
        fields = "__all__"
