from rest_framework import serializers

from ..models import ProductMedia
from ...core.serializer_fields import MultiImageField


class ProductMediaSerializer(serializers.ModelSerializer):
    image_display = MultiImageField()
    image_preview = MultiImageField()

    class Meta:
        model = ProductMedia
        exclude = (
            "product",
            "image",
        )
