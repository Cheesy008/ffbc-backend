from rest_framework import serializers

from ..models import MeasureField, MeasureCategory


class MeasureFieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = MeasureField
        fields = "__all__"


class MeasureCategorySerializer(serializers.ModelSerializer):
    measure_fields = MeasureFieldSerializer(many=True)

    class Meta:
        model = MeasureCategory
        fields = "__all__"
