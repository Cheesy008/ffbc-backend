from rest_framework import serializers

from .product_media_serializers import ProductMediaSerializer
from .product_type_serializers import ProductTypeSerializer
from ..models import Product


class ProductListSerializer(serializers.ModelSerializer):
    product_type = ProductTypeSerializer()
    product_media = ProductMediaSerializer(many=True)

    class Meta:
        model = Product
        fields = (
            "id",
            "name",
            "description",
            "base_price",
            "product_type",
            "product_media",
        )


class ProductDetailSerializer(serializers.ModelSerializer):
    product_type = ProductTypeSerializer()
    product_media = ProductMediaSerializer(many=True)

    class Meta:
        model = Product
        fields = "__all__"
