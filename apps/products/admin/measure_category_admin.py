from django.contrib import admin

from ..models import MeasureCategory, MeasureField


class MeasureFieldInline(admin.TabularInline):
    model = MeasureField
    extra = 0


@admin.register(MeasureCategory)
class MeasureCategoryAdmin(admin.ModelAdmin):
    inlines = [MeasureFieldInline]
