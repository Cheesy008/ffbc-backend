from django.contrib import admin

from ..models import ProductType


@admin.register(ProductType)
class ProductTypeAdmin(admin.ModelAdmin):
    pass
