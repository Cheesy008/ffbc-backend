from django.contrib import admin

from ..models import ProductFeature, ProductFeatureField


class ProductFeatureFieldInline(admin.TabularInline):
    model = ProductFeatureField
    extra = 1


@admin.register(ProductFeature)
class ProductFeatureAdmin(admin.ModelAdmin):
    inlines = [ProductFeatureFieldInline]
