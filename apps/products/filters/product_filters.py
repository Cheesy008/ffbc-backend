from django_filters import ChoiceFilter
from django_filters import rest_framework as filters

from ..models import Product

LEVEL_1 = 1
LEVEL_2 = 2
LEVEL_3 = 3

BUDGET_LEVEL_CHOICES = (
    (LEVEL_1, "Level 1"),  # от 0$ до 250$
    (LEVEL_2, "Level 2"),  # от 251$ до 500$
    (LEVEL_3, "Level 3"),  # от 501$
)


class ProductsFilter(filters.FilterSet):
    budget_level = ChoiceFilter(
        label="Уровень бюджета",
        choices=BUDGET_LEVEL_CHOICES,
        method="budget_level_filter",
    )

    class Meta:
        model = Product
        fields = ["name", "product_type"]

    @staticmethod
    def budget_level_filter(queryset, name, value):
        value = int(value)
        if value == LEVEL_1:
            return queryset.filter(base_price__gte=0, base_price__lte=250)
        if value == LEVEL_2:
            return queryset.filter(base_price__gte=251, base_price__lte=500)
        if value == LEVEL_3:
            return queryset.filter(base_price__gte=501)
        return queryset
