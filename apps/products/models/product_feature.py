from django.db import models


class ProductFeature(models.Model):
    """
    Модель названия общих особенностей товара.
    """

    name = models.CharField("Name", max_length=200)
    icon = models.FileField(
        "Icon",
        upload_to="product_feature/i",
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "Product feature"
        verbose_name_plural = "Products features"

    def __str__(self) -> str:
        return self.name
