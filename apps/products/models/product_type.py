from django.db import models


class ProductType(models.Model):
    name = models.CharField("Name", max_length=200)
    measure_categories = models.ManyToManyField(
        "products.MeasureCategory",
        related_name="product_types",
        verbose_name="Measure categories",
        blank=True,
    )
    product_features = models.ManyToManyField(
        "products.ProductFeature",
        related_name="product_types",
        verbose_name="Product features",
        blank=True,
    )

    class Meta:
        verbose_name = "Product type"
        verbose_name_plural = "Product types"

    def __str__(self) -> str:
        return self.name
