from django.db import models


class MeasureCategory(models.Model):
    """
    Модель категории мерок.
    """

    name = models.CharField("Name", max_length=200)

    class Meta:
        verbose_name = "Measure category"
        verbose_name_plural = "Measure categories"

    def __str__(self) -> str:
        return self.name
