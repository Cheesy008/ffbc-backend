from django.core.validators import FileExtensionValidator
from django.db import models


class Product(models.Model):
    """
    Модель товара, которая является шаблоном.
    """

    name = models.CharField("Name", max_length=200)
    base_price = models.DecimalField("Base price", max_digits=15, decimal_places=2, default=0.0)
    description = models.TextField("Description", null=True, blank=True)
    product_type = models.ForeignKey(
        "products.ProductType",
        on_delete=models.CASCADE,
        verbose_name="Product type",
        related_name="products",
        null=True,
        blank=True,
    )
    video = models.FileField(
        upload_to="product/video",
        null=True,
        blank=True,
        validators=[
            FileExtensionValidator(allowed_extensions=["MOV", "avi", "mp4", "webm", "mkv"])
        ],
    )

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"

    def __str__(self) -> str:
        return self.name
