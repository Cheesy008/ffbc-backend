from django.db import models


class MeasureField(models.Model):
    """
    Модель значения мерки.
    """

    measure_category = models.ForeignKey(
        "products.MeasureCategory",
        on_delete=models.CASCADE,
        related_name="measure_fields",
    )
    name = models.CharField("Name", max_length=200)

    class Meta:
        verbose_name = "Measure field"
        verbose_name_plural = "Measure fields"

    def __str__(self) -> str:
        return self.name
