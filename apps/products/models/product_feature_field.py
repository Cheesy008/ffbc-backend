from django.db import models


class ProductFeatureField(models.Model):
    """
    Модель конкретного поля особенности товара.
    """

    name = models.CharField("Name", max_length=200)
    feature = models.ForeignKey(
        "products.ProductFeature",
        on_delete=models.CASCADE,
        related_name="feature_fields",
        verbose_name="Product feature",
    )
    feature_price = models.DecimalField(
        "Feature price",
        max_digits=15,
        decimal_places=2,
        default=0.0,
    )

    class Meta:
        verbose_name = "Product feature"
        verbose_name_plural = "Products features"

    def __str__(self) -> str:
        return self.name
