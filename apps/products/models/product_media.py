from django.db import models

from apps.core.models import PreviewImageField, MultiImageMeta, Spec


class ProductMedia(models.Model, metaclass=MultiImageMeta):
    """
    Модель фото товара.
    """

    product = models.ForeignKey(
        "products.Product",
        on_delete=models.CASCADE,
        verbose_name="Product",
        null=True,
        blank=True,
        related_name="product_media",
    )
    image = PreviewImageField(
        "Image",
        upload_to="product/media",
    )

    image_map = {
        "image_display": Spec(source="image", width=400, height=400),
        "image_preview": Spec(source="image", width=400, height=400, blur=True),
    }

    class Meta:
        verbose_name = "Product media"
        verbose_name_plural = "Products media"

    def __str__(self) -> str:
        return f"{self.product.name} media"
