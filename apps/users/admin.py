from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt import token_blacklist

from .models import User, ContactService, UserContactService, UserDocumentPhoto, UserMeasure


class UserDocumentPhotoInline(admin.TabularInline):
    model = UserDocumentPhoto
    extra = 1


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    ("first_name", "last_name"),
                    "birth_date",
                    ("email", "phone_number"),
                    "avatar",
                    "uae_id",
                    "passport_id",
                    "street",
                    "city",
                    "country",
                    "state",
                    "instagram_url",
                    "height",
                    "weight",
                )
            },
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_verified",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                )
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    actions = ["save"]
    inlines = (UserDocumentPhotoInline,)

    def save(self, request, queryset):
        [x.save() for x in queryset]


admin.site.register(User, CustomUserAdmin)
admin.site.register(ContactService)
admin.site.register(UserContactService)
admin.site.register(UserMeasure)


class OutstandingTokenAdmin(token_blacklist.admin.OutstandingTokenAdmin):
    def has_delete_permission(self, *args, **kwargs):
        return True


admin.site.unregister(token_blacklist.models.OutstandingToken)
admin.site.register(token_blacklist.models.OutstandingToken, OutstandingTokenAdmin)
