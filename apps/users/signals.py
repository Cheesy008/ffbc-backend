from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import User
from ..orders.models import OrderUserTemplate, Cart


@receiver(post_save, sender=User)
def create_order_user_template(sender, instance, created, **kwargs):
    if created:
        OrderUserTemplate.objects.create(user=instance)


@receiver(post_save, sender=User)
def create_user_cart(sender, instance, created, **kwargs):
    if created:
        Cart.objects.create(user=instance)
