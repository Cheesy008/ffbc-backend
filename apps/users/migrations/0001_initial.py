# Generated by Django 3.2.4 on 2022-03-07 13:44

import apps.core.models
from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Email')),
                ('phone_number', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, null=True, region=None, unique=True, verbose_name='Phone number')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='Date of birth')),
                ('avatar', apps.core.models.PreviewImageField(blank=True, null=True, upload_to='users/avatars/', verbose_name='Avatar')),
                ('uae_id', models.CharField(blank=True, max_length=100, null=True, verbose_name='UAE ID')),
                ('passport_id', models.CharField(blank=True, max_length=100, null=True, verbose_name='Passport ID')),
                ('street', models.TextField(blank=True, null=True, verbose_name='Street')),
                ('city', models.TextField(blank=True, null=True, verbose_name='City')),
                ('country', models.CharField(blank=True, max_length=200, null=True, verbose_name='Country')),
                ('state', models.TextField(blank=True, null=True, verbose_name='State')),
                ('instagram_url', models.URLField(blank=True, null=True, verbose_name='Instagram URL')),
                ('height', models.FloatField(blank=True, null=True, verbose_name='Height')),
                ('weight', models.FloatField(blank=True, null=True, verbose_name='Weight')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='ContactService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='Name')),
            ],
            options={
                'verbose_name': 'Contact service',
                'verbose_name_plural': 'Contact services',
            },
        ),
        migrations.CreateModel(
            name='UserDocumentPhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', apps.core.models.PreviewImageField(null=True, upload_to='users/document-photo/', verbose_name='Document photo')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='document_photos', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Document photo',
                'verbose_name_plural': 'Document photos',
            },
        ),
        migrations.CreateModel(
            name='UserContactService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(default='', max_length=200, verbose_name='Value')),
                ('contact_service', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='users.contactservice', verbose_name='Contact service')),
                ('user', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'User contact service',
                'verbose_name_plural': 'User contact services',
                'unique_together': {('contact_service', 'user')},
            },
        ),
        migrations.AddField(
            model_name='user',
            name='contact_services',
            field=models.ManyToManyField(blank=True, through='users.UserContactService', to='users.ContactService', verbose_name='User contact services'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
    ]
