from django.db import models


class ContactService(models.Model):
    """
    Модель сервисов
    """

    name = models.CharField("Name", max_length=200)

    class Meta:
        verbose_name = "Contact service"
        verbose_name_plural = "Contact services"

    def __str__(self):
        return self.name
