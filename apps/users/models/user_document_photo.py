from django.core.exceptions import ValidationError
from django.db import models

from apps.core.models import PreviewImageField


class UserDocumentPhoto(models.Model):
    user = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        related_name="document_photos",
        verbose_name="User",
    )
    image = PreviewImageField(
        "Document photo",
        upload_to="users/document-photo/",
        null=True,
    )

    class Meta:
        verbose_name = "Document photo"
        verbose_name_plural = "Document photos"

    def clean(self) -> None:
        if self.user.document_photos.count() >= 3:
            raise ValidationError("User can't have more than 3 document photos")
        return super().clean()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()
