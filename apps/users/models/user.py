from datetime import date

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import AbstractUser
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from rest_framework_simplejwt.tokens import RefreshToken

from apps.core.enums import BustType
from apps.core.models import PreviewImageField
from apps.users.managers import CustomUserManager


class User(AbstractUser):
    objects = CustomUserManager()

    is_verified = models.BooleanField("Is verified", default=False)
    email = models.EmailField("Email", unique=True)
    phone_number = PhoneNumberField(
        "Phone number",
        null=True,
        blank=True,
        unique=True,
    )
    birth_date = models.DateField("Date of birth", null=True, blank=True)
    avatar = PreviewImageField(
        "Avatar",
        upload_to="users/avatars/",
        null=True,
        blank=True,
    )
    uae_id = models.CharField("UAE ID", max_length=100, null=True, blank=True)
    passport_id = models.CharField("Passport ID", max_length=100, null=True, blank=True)
    contact_services = models.ManyToManyField(
        "users.ContactService",
        through="users.UserContactService",
        blank=True,
        verbose_name="User contact services",
    )
    street = models.TextField("Street", null=True, blank=True)
    city = models.TextField("City", null=True, blank=True)
    country = models.CharField("Country", max_length=200, null=True, blank=True)
    state = models.TextField("State", null=True, blank=True)
    instagram_url = models.URLField("Instagram URL", null=True, blank=True)
    height = models.FloatField("Height", null=True, blank=True)
    weight = models.FloatField("Weight", null=True, blank=True)
    zip_code = models.CharField("Zip code", null=True, blank=True, max_length=100)
    bust_type = models.PositiveSmallIntegerField(
        "Bust type",
        choices=BustType.choices,
        default=BustType.NATURAL,
    )

    def save(self, *args, **kwargs):
        if not self.username:
            self.username = self.email
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.email

    def change_password(self, password):
        self.set_password(password)
        self.save()

    def get_tokens_for_user(self):
        refresh = RefreshToken.for_user(self)
        return str(refresh), str(refresh.access_token)

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}" if self.first_name and self.last_name else ""

    @property
    def age(self):
        return relativedelta(date.today(), self.birth_date).years
