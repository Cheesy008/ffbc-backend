from django.db import models


class UserMeasure(models.Model):
    """
    Модель мерок пользователя.
    """

    user = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="measures",
        verbose_name="User",
    )
    measure_field = models.ForeignKey(
        "products.MeasureField",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Measure field",
        related_name="+",
    )
    value = models.TextField("Value", null=True, blank=True)

    class Meta:
        verbose_name = "User measure"
        verbose_name_plural = "User measures"
