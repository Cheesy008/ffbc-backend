from django.contrib.auth import get_user_model
from django.db import transaction
from rest_framework import serializers

from apps.users.models import UserDocumentPhoto
from .contact_service import UserContactServiceSerializer
from ..services import update_user

User = get_user_model()


class UserDocumentPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDocumentPhoto
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    """
    Сериалайзер для вывода и редактирования данных пользователя.
    """

    document_photos = UserDocumentPhotoSerializer(many=True, read_only=True)
    remove_document_photos = serializers.ListField(
        child=serializers.IntegerField(),
        write_only=True,
        required=False,
    )
    contact_services = UserContactServiceSerializer(
        source="usercontactservice_set", many=True, read_only=True
    )

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "birth_date",
            "document_photos",
            "remove_document_photos",
            "avatar",
            "uae_id",
            "passport_id",
            "contact_services",
            "street",
            "city",
            "country",
            "state",
            "instagram_url",
            "height",
            "weight",
            "bust_type",
        )
        extra_kwargs = {
            "password": {"required": False, "write_only": True},
        }

    def get_age(self, instance):
        return instance.age or None

    @transaction.atomic
    def update(self, instance, validated_data):
        validated_data.pop("document_photos", "")
        for i in self.context["request"].FILES.getlist("document_photos", []):
            document = UserDocumentPhotoSerializer(data={"image": i, "user": instance.id})
            document.is_valid(raise_exception=True)
            document.save()
        remove_files = validated_data.pop("remove_document_photos", [])
        if remove_files:
            UserDocumentPhoto.objects.filter(user_id=instance.id, id__in=remove_files).delete()
        return update_user(instance=instance, user_data=validated_data)
