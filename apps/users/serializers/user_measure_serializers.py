from rest_framework import serializers

from ..models import UserMeasure


class UserMeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMeasure
        fields = "__all__"
