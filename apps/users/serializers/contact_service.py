from rest_framework import serializers

from ..models import ContactService, UserContactService


class ContactServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactService
        fields = "__all__"


class UserContactServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserContactService
        fields = "__all__"
        extra_kwargs = {"user": {"read_only": True}}

    def to_internal_value(self, data):
        res = super().to_internal_value(data)
        res["user"] = self.context["request"].user
        return res
