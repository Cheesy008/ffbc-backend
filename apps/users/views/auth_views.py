from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from apps.core.serializers import ApiErrorsMixin
from apps.core.throttles import SendEmailRateThrottle
from ..serializers import (
    LoginSerializer,
    LogoutSerializer,
    PasswordResetConfirmSerializer,
    RegistrationSerializer,
    RequestResetPasswordSerializer,
    ResendVerificationLinkSerializer,
    SetNewPasswordSerializer,
    UpdatePasswordSerializer,
    VerifyUserEmailSerializer,
)

User = get_user_model()


class AuthViewSet(ApiErrorsMixin, viewsets.ViewSet):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        method="post",
        request_body=RegistrationSerializer(),
        responses={200: "Activation link was sent to your email"},
    )
    @action(detail=False, methods=["post"])
    def register(self, request, *args, **kwargs):
        """
        Регистрация и отправка письма о подтверждении на емайл.
        """
        serializer = RegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("Activation link was sent to your email")}, status.HTTP_200_OK
        )

    @swagger_auto_schema(
        method="post",
        request_body=VerifyUserEmailSerializer(),
        responses={200: "Email has been successfully verified"},
    )
    @action(detail=False, methods=["post"], url_path="verify-email")
    def verify_email(self, request, *args, **kwargs):
        """
        Подтверждение емайла пользователя.
        """
        serializer = VerifyUserEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({"message": _("Email has been successfully verified")}, status.HTTP_200_OK)

    @swagger_auto_schema(
        method="post",
        request_body=ResendVerificationLinkSerializer(),
        responses={200: "The activation link has been sent to your email again"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="resend-verification-link",
    )
    def resend_verification_link(self, request, *args, **kwargs):
        """
        Повторная отправки ссылки на подтверждение пользователя.
        """
        serializer = ResendVerificationLinkSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The activation link has been sent to your email again")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=RequestResetPasswordSerializer(),
        responses={200: "The link to reset password has been sent to your email"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="request-reset-password",
        throttle_classes=[SendEmailRateThrottle],
    )
    def request_reset_password(self, request, *args, **kwargs):
        """
        Запрос о сбросе пароля.
        """
        serializer = RequestResetPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The reset password link has been sent to your email")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=PasswordResetConfirmSerializer(),
        responses={200: PasswordResetConfirmSerializer()},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="password-reset-confirm",
    )
    def password_reset_confirm(self, request, *args, **kwargs):
        """
        Подтверждение о сбросе пароля.
        """
        serializer = PasswordResetConfirmSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.validated_data, status.HTTP_200_OK)

    @swagger_auto_schema(
        method="post",
        request_body=SetNewPasswordSerializer(),
        responses={200: "The password was updated successfully"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="set-new-password",
    )
    def set_new_password(self, request, *args, **kwargs):
        """
        Установка нового пароля.
        """
        serializer = SetNewPasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The password was updated successfully")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=UpdatePasswordSerializer(),
        responses={200: "The password was updated successfully"},
    )
    @action(
        detail=False,
        methods=["post"],
        url_path="update-user-password",
        permission_classes=[IsAuthenticated],
    )
    def update_user_password(self, request, *args, **kwargs):
        """
        Обновление пароля самим пользователем.
        """
        serializer = UpdatePasswordSerializer(data=request.data, context={"user": request.user})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            {"message": _("The password was updated successfully")},
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(method="post", request_body=LoginSerializer())
    @action(detail=False, methods=["post"])
    def login(self, request, *args, **kwargs):
        """
        Авторизация на сайт.
        """
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status.HTTP_200_OK)

    @swagger_auto_schema(method="post", request_body=LogoutSerializer(), responses={204: "Logout"})
    @action(detail=False, methods=["post"], permission_classes=[IsAuthenticated])
    def logout(self, request, *args, **kwargs):
        """
        Выход с сайта.
        """
        serializer = LogoutSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_204_NO_CONTENT)
