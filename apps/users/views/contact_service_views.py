from rest_framework import viewsets
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated

from ..models import ContactService, UserContactService
from ..serializers import ContactServiceSerializer, UserContactServiceSerializer
from ...core.permissions import IsVerified, IsOwner
from ...core.serializers import ApiErrorsMixin, CreateListModelMixin


class ContactServiceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ContactService.objects.all()
    serializer_class = ContactServiceSerializer
    pagination_class = None


class UserContactServiceViewSet(
    ApiErrorsMixin,
    CreateListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    viewsets.GenericViewSet,
):
    queryset = UserContactService.objects.all()
    serializer_class = UserContactServiceSerializer
    permission_classes = (IsAuthenticated, IsVerified, IsOwner)
