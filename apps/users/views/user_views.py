from django.contrib.auth import get_user_model
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.competitions.models import Competition, ViewerTicket
from apps.competitions.serializers import (
    CompetitionUserInfoSerializer,
    ViewerTicketSerializer,
)
from apps.core.permissions import IsVerified, IsOwner
from apps.core.serializers import ApiErrorsMixin, CreateListModelMixin
from ..models import UserMeasure
from ..serializers import (
    UserSerializer,
)
from ..serializers.user_measure_serializers import UserMeasureSerializer

User = get_user_model()


class UserViewset(ApiErrorsMixin, viewsets.ViewSet):
    permission_classes = (
        IsAuthenticated,
        IsVerified,
    )

    @swagger_auto_schema(method="get", responses={200: UserSerializer()})
    @action(methods=["get"], detail=False, url_path="me", url_name="current_user")
    def current_user(self, request, *args, **kwargs):
        """
        Получение данных текущего пользователя.
        """
        return Response(
            UserSerializer(request.user, context={"request": request}).data,
            status.HTTP_200_OK,
        )

    @swagger_auto_schema(
        method="post",
        request_body=UserSerializer(),
        responses={200: UserSerializer()},
    )
    @action(methods=["post"], detail=False, url_path="me/edit", url_name="edit_current_user")
    def edit_current_user(self, request, *args, **kwargs):
        """
        Редактирование данных текущего пользователя.
        """
        serializer = UserSerializer(request.user, data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="get",
        responses={200: ViewerTicketSerializer()},
    )
    @action(methods=["get"], detail=False, url_path="me/tickets")
    def get_tickets(self, request, *args, **kwargs):
        """
        Получение билетов на соревнования в качестве зрителя.
        """
        queryset = ViewerTicket.objects.filter(user=request.user)
        serializer = ViewerTicketSerializer(queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method="get",
        responses={200: CompetitionUserInfoSerializer()},
    )
    @action(methods=["get"], detail=False, url_path="me/competition-registrations")
    def get_competition_registrations(self, request, *args, **kwargs):
        """
        Получение записей на соревнование в качестве участника.
        """
        queryset = Competition.objects.filter(
            competition_categories__competition_registrations__user=request.user
        )
        serializer = CompetitionUserInfoSerializer(queryset, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)


class UserMeasureViewSet(ApiErrorsMixin, CreateListModelMixin, viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, IsVerified, IsOwner)
    serializer_class = UserMeasureSerializer
    pagination_class = None

    def get_queryset(self):
        return UserMeasure.objects.filter(user=self.request.user)
