import jwt
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.exceptions import ValidationError
from django.utils.encoding import (
    force_str,
    smart_bytes,
)
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from rest_framework.generics import get_object_or_404
from rest_framework_simplejwt.tokens import RefreshToken

from apps.users.models import UserDocumentPhoto
from apps.users.tasks import send_email_task
from config.settings import (
    FRONTEND_RESET_PASSWORD_REDIRECT_URL,
    FRONTEND_VERIFY_USER_EMAIL_REDIRECT_URL,
)

User = get_user_model()


# region Auth
def send_activation_link(*, user: User, redirect_url: str) -> None:
    """
    Формирование ссылки для подтверждения пользователя и отправка письма
    на почту.

    Токен формируется при помощи метода из библиотеки rest_framework_simplejwt,
    после чего вставляется внутрь ссылки в качестве query параметра.
    """
    token = RefreshToken.for_user(user).access_token
    redirect_url = redirect_url if redirect_url else FRONTEND_VERIFY_USER_EMAIL_REDIRECT_URL

    absolute_url = f"{redirect_url}?token={str(token)}"
    email_body = f"Verify it {absolute_url}"
    data = {
        "email_body": email_body,
        "to_email": user.email,
        "email_subject": "Verify your email",
    }

    send_email_task.delay(data)


def verify_user_email(*, token: str) -> None:
    """
    Верификация емайла пользователя.

    Получаем token, взятый из query параметра, затем
    декодируем. При удачной попытке подтверждаем
    пользователя, иначе кидаем exception.
    """
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=["HS256"])
        user = User.objects.get(id=payload["user_id"])
        if not user.is_verified:
            user.is_verified = True
            user.save()
    except jwt.ExpiredSignatureError as identifier:
        raise ValidationError(_("Activation expired"))
    except jwt.exceptions.DecodeError as identifier:
        raise ValidationError(_("Invalid token"))


def send_reset_password_link(*, email: str, redirect_url: str) -> None:
    """
    Отправка ссылки для сброса пароля.

    Для формирования ссылки кодируем id пользователя в base64
    и генерируем токен из пользователя. Указываем redirect_url
    для направления пользователя на сайт.
    """
    user = User.objects.find_by_email(email)

    if user is None:
        raise ValidationError(_("User doesn't exist"))

    uidb64 = urlsafe_base64_encode(smart_bytes(user.id))
    token = PasswordResetTokenGenerator().make_token(user)
    redirect_url = redirect_url if redirect_url else FRONTEND_RESET_PASSWORD_REDIRECT_URL
    absolute_url = f"{redirect_url}?uidb64={uidb64}&token={token}"
    email_body = f"Use this link to reset your password {absolute_url}"

    data = {
        "email_body": email_body,
        "to_email": email,
        "email_subject": "Reset your passsword",
    }

    send_email_task.delay(data)


def decode_and_find_user(uidb64: str) -> User:
    """
    Декодируем uidb64 строку, получаем id пользователя
    и находим его, иначе кидаем exception.
    """
    try:
        id = force_str(urlsafe_base64_decode(uidb64))
        return User.objects.find_by_id(id)
    except Exception as e:
        raise ValidationError("Invalid uidb64")


def check_reset_password_token(*, uidb64: str, token: str) -> None:
    """
    Валидируем токен для сброса пароля.
    """
    user = decode_and_find_user(uidb64)

    if not PasswordResetTokenGenerator().check_token(user, token):
        raise ValidationError("Token is invalid")


def set_new_password(*, password: str, token: str, uidb64: str) -> None:
    """
    При успешной валидации токена устанавливаем новый пароль пользователю.
    """
    user = decode_and_find_user(uidb64)

    if not PasswordResetTokenGenerator().check_token(user, token):
        raise ValidationError("Token is invalid")

    user.set_password(password)
    user.save()


def update_user_password(*, password: str, user: User) -> None:
    """
    Установка нового пароля пользователя
    """
    user.set_password(password)
    user.save()


# endregion

# region User
def update_user(*, instance: User, user_data: dict) -> User:
    """
    Обновляем учетные данные в моделях User.
    """
    contact_services = user_data.pop("contact_services", [])

    instance.contact_services.set(contact_services)

    for attr, value in user_data.items():
        setattr(instance, attr, value)

    instance.save()

    return instance


def upload_user_avatar(*, user: User, image) -> None:
    """
    Загрузка аватара пользователя.
    """
    user.avatar = image
    user.save()


def upload_document_photo(*, user: User, image):
    """
    Загрузка фото для документа пользователя.
    """
    document_photo = UserDocumentPhoto.objects.create(image=image, user=user)
    return document_photo.image


def delete_document_photo(*, id: int):
    """
    Удаление фото для документа пользователя.
    """
    document_photo = get_object_or_404(UserDocumentPhoto, id=id)
    document_photo.delete()


# endregion
