from django.contrib.auth.models import UserManager


class CustomUserManager(UserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError("Email field is required")

        if not kwargs.get("username", None):
            kwargs["username"] = email

        email = self.normalize_email(email)
        user = self.model(email=email, **kwargs)
        user.set_password(password)
        user.save()

        return user

    def find_by_email(self, email):
        return self.get_queryset().filter(email=email).first()

    def find_by_id(self, id):
        return self.get_queryset().filter(id=id).first()
