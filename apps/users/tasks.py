from django.core.mail import EmailMessage

from config.celery import app


@app.task
def send_email_task(data):
    email = EmailMessage(
        subject=data["email_subject"],
        body=data["email_body"],
        to=[data["to_email"]],
    )
    email.send()
