from django.db.models import Sum

from apps.orders.models import OrderItem, CartItem


def calculate_total_item_price(instance: OrderItem | CartItem):
    instance.price = (
        instance.product.base_price
        + instance.feature_fields.aggregate(total_price=Sum("feature_price"))["total_price"]
    )
    instance.save()
