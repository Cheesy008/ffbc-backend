# Generated by Django 3.2.4 on 2022-03-10 18:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
        ('orders', '0010_auto_20220309_0744'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartitem',
            name='feature_fields',
            field=models.ManyToManyField(blank=True, related_name='_orders_cartitem_feature_fields_+', to='products.ProductFeatureField', verbose_name='Product feature fields'),
        ),
    ]
