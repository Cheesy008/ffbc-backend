# Generated by Django 3.2.4 on 2022-03-09 07:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_usermeasure'),
        ('orders', '0008_alter_order_order_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderContactService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(default='', max_length=200, verbose_name='Value')),
                ('contact_service', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='users.contactservice', verbose_name='Contact service')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contact_services', to='orders.order', verbose_name='Order')),
            ],
            options={
                'verbose_name': 'Order contact service',
                'verbose_name_plural': 'Order contact services',
                'unique_together': {('contact_service', 'order')},
            },
        ),
    ]
