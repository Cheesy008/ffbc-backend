from rest_framework import serializers

from ..models import OrderContactService


class OrderContactServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderContactService
        fields = "__all__"
