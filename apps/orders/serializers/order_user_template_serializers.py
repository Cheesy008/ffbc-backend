from rest_framework import serializers

from ..models import OrderUserTemplate


class OrderUserTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderUserTemplate
        fields = "__all__"
