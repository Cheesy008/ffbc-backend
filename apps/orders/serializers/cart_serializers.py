from rest_framework import serializers

from ..models import Cart, CartItem
from ..services import calculate_total_item_price


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = "__all__"
        extra_kwargs = {"price": {"read_only": True}}

    def create(self, validated_data):
        instance: CartItem = super().create(validated_data)
        calculate_total_item_price(instance)
        return instance


class CartSerializer(serializers.ModelSerializer):
    cart_items = CartItemSerializer(many=True)

    class Meta:
        model = Cart
        fields = "__all__"


class CartDeleteSerializer(serializers.Serializer):
    ids = serializers.ListField(child=serializers.IntegerField())
