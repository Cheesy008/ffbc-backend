from rest_framework import serializers

from ..models import OrderAdditionalPhoto


class CreateOrderAdditionalPhotoSerializer(serializers.ModelSerializer):
    photo = serializers.ListField(
        child=serializers.ImageField(max_length=100000, allow_empty_file=False),
        required=True,
        min_length=1,
    )

    class Meta:
        model = OrderAdditionalPhoto
        fields = "__all__"

    def create(self, validated_data):
        photos = [
            OrderAdditionalPhoto(photo=p, order=validated_data["order"])
            for p in validated_data["photo"]
        ]
        return OrderAdditionalPhoto.objects.bulk_create(photos)


class OrderAdditionalPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderAdditionalPhoto
        exclude = ("order",)
