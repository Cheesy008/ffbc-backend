from rest_framework import serializers

from ..models import OrderItem
from ..services import calculate_total_item_price


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = "__all__"
        extra_kwargs = {"price": {"read_only": True}}

    def create(self, validated_data):
        instance: OrderItem = super().create(validated_data)
        calculate_total_item_price(instance)
        return instance
