from rest_framework import serializers

from ..models import OrderMeasure


class OrderMeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderMeasure
        fields = "__all__"
