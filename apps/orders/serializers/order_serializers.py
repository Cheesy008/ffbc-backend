from rest_framework import serializers

from .order_additional_photo_serializer import OrderAdditionalPhotoSerializer
from .order_contact_service_serializers import OrderContactServiceSerializer
from .order_item_serializers import OrderItemSerializer
from .order_measure_serializers import OrderMeasureSerializer
from ..models import Order


class OrderSerializer(serializers.ModelSerializer):
    additional_photos = OrderAdditionalPhotoSerializer(many=True, read_only=True)
    order_items = OrderItemSerializer(many=True, read_only=True)
    measures = OrderMeasureSerializer(many=True, read_only=True)
    contact_services = OrderContactServiceSerializer(
        source="ordercontactservice_set", many=True, read_only=True
    )

    class Meta:
        model = Order
        fields = "__all__"
        extra_kwargs = {
            "order_status": {"read_only": True},
            "payment_date": {"read_only": True},
        }
