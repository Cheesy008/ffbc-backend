from django.db import models


class OrderContactService(models.Model):
    value = models.CharField("Value", max_length=200, default="")
    contact_service = models.ForeignKey(
        "users.ContactService",
        on_delete=models.CASCADE,
        verbose_name="Contact service",
        blank=True,
    )
    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        verbose_name="Order",
    )

    class Meta:
        verbose_name = "Order contact service"
        verbose_name_plural = "Order contact services"
        unique_together = ("contact_service", "order")

    def __str__(self):
        return self.value
