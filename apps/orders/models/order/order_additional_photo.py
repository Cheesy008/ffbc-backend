from django.db import models

from apps.core.models import PreviewImageField


class OrderAdditionalPhoto(models.Model):
    """
    Модель дополнительных фото к заказу.
    """

    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="additional_photos",
        verbose_name="Order",
    )
    photo = PreviewImageField(
        "Photo",
        upload_to="order_additional_photo/p",
    )

    class Meta:
        verbose_name = "Order additional photo"
        verbose_name_plural = "Order additional photos"
