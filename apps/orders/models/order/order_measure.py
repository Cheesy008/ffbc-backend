from django.db import models


class OrderMeasure(models.Model):
    """
    Модель мерок заказа.
    """

    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="measures",
        verbose_name="Order",
    )
    measure_field = models.ForeignKey(
        "products.MeasureField",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="Measure field",
        related_name="+",
    )
    value = models.TextField("Value", null=True, blank=True)

    class Meta:
        verbose_name = "Order measure"
        verbose_name_plural = "Order measures"
