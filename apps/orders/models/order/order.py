from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from apps.core.enums import BustType, OrderStatusType
from apps.core.models import PreviewImageField


class Order(models.Model):
    """
    Модель заказа пользователя.
    """

    user = models.ForeignKey(
        "users.User",
        on_delete=models.CASCADE,
        verbose_name="User",
        null=True,
        blank=True,
        related_name="orders",
    )
    first_name = models.CharField("First name", max_length=100)
    last_name = models.CharField("Last name", max_length=100)
    email = models.EmailField("Email", null=True, blank=True)
    street = models.TextField("Street", null=True, blank=True)
    city = models.TextField("City", null=True, blank=True)
    country = models.CharField("Country", max_length=120, null=True, blank=True)
    state = models.CharField("State", max_length=120, null=True, blank=True)
    zip_code = models.CharField("Zip code", max_length=100)
    phone_number = PhoneNumberField("Phone number", null=True, blank=True)
    instagram_url = models.URLField("Instagram URL", null=True, blank=True)
    birth_date = models.DateField("Date of birth", null=True, blank=True)
    height = models.PositiveIntegerField("Height", null=True, blank=True)
    competition_description = models.TextField("Competition description", null=True, blank=True)
    bust_type = models.PositiveSmallIntegerField(
        "Bust type", choices=BustType.choices, default=BustType.NATURAL
    )
    contacts = models.TextField("Contacts", null=True, blank=True)
    ordered_date = models.DateTimeField("Ordered date", auto_now=True)
    payment_date = models.DateTimeField("Payment date", null=True, blank=True)
    order_status = models.PositiveSmallIntegerField(
        "Order status",
        choices=OrderStatusType.choices,
        default=OrderStatusType.NOT_CONFIRMED,
    )
    contact_services = models.ManyToManyField(
        "users.ContactService",
        through="orders.OrderContactService",
        blank=True,
        verbose_name="Order contact services",
    )

    # Дополнительная информация
    weight = models.PositiveSmallIntegerField("Weight", null=True, blank=True)
    full_size_photo = PreviewImageField(
        "Full size photo",
        upload_to="orders/fsp",
        null=True,
        blank=True,
    )
    back_pose_photo = PreviewImageField(
        "Back pose photo",
        upload_to="orders/bpp",
        null=True,
        blank=True,
    )
    side_pose_photo = PreviewImageField(
        "Side pose photo",
        upload_to="orders/spp",
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name} #{self.id}"

    @property
    def total_price(self):
        return sum([item.price for item in self.order_items.all()])
