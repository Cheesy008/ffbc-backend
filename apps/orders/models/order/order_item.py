from django.db import models


class OrderItem(models.Model):
    """
    Модель единицы заказа, в котором товар делается индивидуально для пользователя.
    """

    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="order_items",
        verbose_name="Order",
    )
    product = models.ForeignKey(
        "products.Product",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="+",
        verbose_name="Product",
    )
    feature_fields = models.ManyToManyField(
        "products.ProductFeatureField",
        related_name="+",
        verbose_name="Product feature fields",
    )
    price = models.DecimalField(
        "Price",
        max_digits=15,
        decimal_places=2,
        default=0.0,
    )

    class Meta:
        verbose_name = "Order item"
        verbose_name_plural = "Order items"
