from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from apps.core.enums import BustType


class OrderUserTemplate(models.Model):
    """
    Модель шаблона заказа для пользователя.
    """

    user = models.OneToOneField(
        "users.User",
        on_delete=models.CASCADE,
        verbose_name="User",
        related_name="order_template",
    )
    first_name = models.CharField("First name", max_length=100)
    last_name = models.CharField("Last name", max_length=100)
    email = models.EmailField("Email", null=True, blank=True)
    address = models.TextField("Address", null=True, blank=True)
    country = models.CharField("Country", max_length=120, null=True, blank=True)
    state = models.CharField("State", max_length=120, null=True, blank=True)
    zip_code = models.CharField("Zip code", max_length=100)
    phone_number = PhoneNumberField("Phone number", null=True, blank=True)
    instagram_url = models.URLField("Instagram URL", null=True, blank=True)
    birth_date = models.DateField("Date of birth", null=True, blank=True)
    height = models.PositiveIntegerField("Height", null=True, blank=True)
    competition_description = models.TextField("Competition description", null=True, blank=True)
    bust_type = models.PositiveSmallIntegerField(
        "Bust type", choices=BustType.choices, default=BustType.NATURAL
    )
    contacts = models.TextField("Contacts", null=True, blank=True)

    class Meta:
        verbose_name = "Order user template"
        verbose_name_plural = "Order user templates"

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"
