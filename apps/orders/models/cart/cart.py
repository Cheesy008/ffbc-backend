from django.db import models


class Cart(models.Model):
    """
    Модель корзины пользователя, который авторизован.
    """

    user = models.OneToOneField(
        "users.User",
        on_delete=models.CASCADE,
        verbose_name="User",
        related_name="cart",
    )

    class Meta:
        verbose_name = "Cart"
        verbose_name_plural = "Carts"

    def __str__(self) -> str:
        return f"{self.user.first_name} {self.user.last_name}"
