from django.db import models


class CartItem(models.Model):
    """
    Модель единицы корзины, в котором товар делается индивидуально для пользователя.
    """

    cart = models.ForeignKey(
        "orders.Cart",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="cart_items",
        verbose_name="Cart",
    )
    product = models.ForeignKey(
        "products.Product",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="+",
        verbose_name="Product",
    )
    feature_fields = models.ManyToManyField(
        "products.ProductFeatureField",
        related_name="+",
        verbose_name="Product feature fields",
        blank=True,
    )
    price = models.DecimalField(
        "Price",
        max_digits=15,
        decimal_places=2,
        default=0.0,
    )

    class Meta:
        verbose_name = "Cart item"
        verbose_name_plural = "Cart items"
