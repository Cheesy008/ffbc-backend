from django.contrib import admin

from ..models import Order, OrderAdditionalPhoto, OrderMeasure, OrderContactService


class OrderAdditionalPhotoInline(admin.TabularInline):
    model = OrderAdditionalPhoto
    extra = 0


class OrderMeasureInline(admin.TabularInline):
    model = OrderMeasure
    extra = 0


class OrderContactServiceInline(admin.TabularInline):
    model = OrderContactService
    extra = 0


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderAdditionalPhotoInline, OrderMeasureInline, OrderContactServiceInline]
