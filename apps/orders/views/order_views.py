from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.core.serializers import ApiErrorsMixin, CreateListModelMixin
from ..filters import OrderFilter
from ..models import Order, OrderItem, OrderAdditionalPhoto, OrderMeasure
from ..serializers import (
    OrderSerializer,
    OrderItemSerializer,
    OrderAdditionalPhotoSerializer,
    CreateOrderAdditionalPhotoSerializer,
    OrderMeasureSerializer,
)
from ...core.enums import OrderStatusType, BustType


class OrderViewSet(
    ApiErrorsMixin,
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    GenericViewSet,
):
    serializer_class = OrderSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = OrderFilter

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Order.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        if self.request.user.is_authenticated:
            serializer.save(user=self.request.user)
        super().perform_create(serializer)

    @action(detail=False)
    def specs(self, request):
        data = {
            "order_status_type": [
                {"value": value, "label": label} for value, label in OrderStatusType.choices
            ],
            "bust_type": [{"value": value, "label": label} for value, label in BustType.choices],
        }
        return Response(data=data)


class OrderItemViewSet(
    ApiErrorsMixin,
    CreateListModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer


class OrderMeasureViewSet(
    ApiErrorsMixin,
    CreateListModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    queryset = OrderMeasure.objects.all()
    serializer_class = OrderMeasureSerializer


class OrderAdditionalPhotoViewSet(
    ApiErrorsMixin,
    CreateModelMixin,
    GenericViewSet,
):
    queryset = OrderAdditionalPhoto.objects.all()

    def get_serializer_class(self):
        if self.action == "create":
            return CreateOrderAdditionalPhotoSerializer
        return OrderAdditionalPhotoSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=201)
