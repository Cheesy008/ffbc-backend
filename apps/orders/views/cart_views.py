from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, CreateModelMixin, DestroyModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.core.serializers import ApiErrorsMixin
from ..models import Cart, CartItem
from ..serializers import CartSerializer, CartItemSerializer, CartDeleteSerializer
from ...core.permissions import IsVerified, IsCartOwner


class CartViewSet(ListModelMixin, GenericViewSet):
    model = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = (
        IsAuthenticated,
        IsVerified,
    )

    def list(self, request, *args, **kwargs):
        user = request.user
        if not hasattr(user, "cart"):
            return Response(status=400, data={"error": "У пользователя нет корзины"})
        cart = Cart.objects.get(user=user)
        serializer = self.serializer_class(instance=cart)
        return Response(status=200, data=serializer.data)


class CartItemViewSet(ApiErrorsMixin, CreateModelMixin, DestroyModelMixin, GenericViewSet):
    model = CartItem.objects.all()
    serializer_class = CartItemSerializer
    permission_classes = (
        IsAuthenticated,
        IsVerified,
        IsCartOwner,
    )

    @swagger_auto_schema(
        method="post",
        request_body=CartDeleteSerializer(),
        responses={200: None},
    )
    @action(detail=False, methods=["POST"], url_path="delete-multiple")
    def delete_multiple(self, request):
        serializer = CartDeleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        CartItem.objects.filter(
            cart__user=request.user, id__in=serializer.validated_data["ids"]
        ).delete()
        return Response(status=200)
