from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.core.serializers import ApiErrorsMixin
from ..models import OrderUserTemplate
from ..serializers import OrderUserTemplateSerializer


class OrderUserTemplateViewSet(ApiErrorsMixin, ModelViewSet):
    queryset = OrderUserTemplate.objects.all()
    serializer_class = OrderUserTemplateSerializer

    @action(detail=False, url_path="current-user", permission_classes=[IsAuthenticated])
    def current_user(self, request):
        instance = OrderUserTemplate.objects.get_or_create(user=request.user)
        serializer = self.serializer_class(instance)
        return Response(status=200, data=serializer.data)
