from rest_framework import viewsets
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin

from apps.core.serializers import ApiErrorsMixin, CreateListModelMixin
from ..models import OrderContactService
from ..serializers import OrderContactServiceSerializer


class OrderContactServiceViewSet(
    ApiErrorsMixin,
    CreateListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    viewsets.GenericViewSet,
):
    queryset = OrderContactService.objects.all()
    serializer_class = OrderContactServiceSerializer
