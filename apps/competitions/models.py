from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.core.enums import TicketType
from apps.core.models import PreviewImageField

User = get_user_model()


class Competition(models.Model):
    """
    Модель соревнований.
    """

    name = models.CharField(_("Competition name"), max_length=150)
    date = models.DateTimeField(_("Date of the competition"), null=True, blank=True)
    location = models.CharField(_("Location"), max_length=150, null=True, blank=True)
    sponsors = models.ManyToManyField(
        "catalogs.Sponsor", verbose_name=_("Sponsors"), related_name="competitions"
    )
    description = models.TextField(_("Description"), null=True, blank=True)
    vip_ticket_price = models.DecimalField(
        _("Ticket price for VIP viewers"), decimal_places=2, max_digits=15, default=0
    )
    regular_ticket_price = models.DecimalField(
        _("Ticket price for regular viewers"),
        decimal_places=2,
        max_digits=15,
        default=0,
    )
    # TODO добавить поле промовидео

    class Meta:
        verbose_name = _("Competition")
        verbose_name_plural = _("Competitions")

    def __str__(self) -> str:
        return self.name


class CompetitionImage(models.Model):
    """
    Модель изображений для соревнования.
    """

    image = PreviewImageField(
        "Gallery image",
        upload_to="competition/image/",
        null=True,
        blank=True,
    )
    competition = models.ForeignKey(
        Competition,
        on_delete=models.CASCADE,
        related_name="images",
        verbose_name=_("Competition"),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("Competition image")
        verbose_name_plural = _("Competition images")

    def __str__(self) -> str:
        return f"Competition image #{self.id}"

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save()


class CompetitionCategory(models.Model):
    """
    Модель категории соревнования, на которую можно записаться.
    """

    name = models.CharField(_("Category name"), max_length=150)
    price = models.DecimalField(_("Price for competitors"), decimal_places=2, max_digits=15)
    competition = models.ForeignKey(
        Competition,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="competition_categories",
        verbose_name=_("Competition"),
    )

    class Meta:
        verbose_name = _("Competition category")
        verbose_name_plural = _("Competition categories")

    def __str__(self) -> str:
        return self.name


class CompetitionRegistration(models.Model):
    """
    Модель записи участника на категорию.
    """

    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="competition_registrations",
        verbose_name=_("User"),
    )
    competition_categories = models.ManyToManyField(
        CompetitionCategory,
        related_name="competition_registrations",
        verbose_name=_("Competition category"),
    )

    class Meta:
        verbose_name = _("Competition registration")
        verbose_name_plural = _("Competition registrations")


class ViewerTicket(models.Model):
    """
    Модель билета зрителя на соревнование.
    """

    ticket_type = models.PositiveSmallIntegerField(
        _("Ticket type"), choices=TicketType.choices, default=TicketType.REGULAR
    )
    user = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name="tickets", verbose_name=_("User")
    )
    competition = models.ForeignKey(
        Competition,
        on_delete=models.PROTECT,
        related_name="tickets",
        verbose_name=_("Competition"),
    )

    class Meta:
        verbose_name = _("Competition ticket")
        verbose_name_plural = _("Competition tickets")


# class TicketPayment(BaseModel):
#     """
#     Модель оплаты билетов.
#     """

#     amount_paid = models.DecimalField(_("Amount paid"), decimal_places=2, max_digits=15)
#     payment_date = models.DateTimeField(_("Date of payment"))
#     competition_ticket = models.OneToOneField(
#         CompetitionTicket,
#         on_delete=models.PROTECT,
#         related_name="ticket_payment",
#         verbose_name=_("Competition ticket"),
#     )

#     class Meta:
#         verbose_name = _("Ticket payment")
#         verbose_name_plural = _("Ticket payments")
