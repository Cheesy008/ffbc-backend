from rest_framework import views, viewsets

from apps.competitions.models import Competition
from .serializers import CompetitionDetailSerializer, CompetitionListSerializer


class CompetitionViewSet(viewsets.ModelViewSet):
    """
    Вьюсет для получения списка/конкретного экземпляра соревнований.
    """

    queryset = Competition.objects.prefetch_related("sponsors", "competition_categories").all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return CompetitionDetailSerializer
        return CompetitionListSerializer


class BuyTicketAPIView(views.APIView):
    """
    API для покупки билетов зрителей.
    """

    def post(self, request):
        pass


class CheckInCompetitorAPIView(views.APIView):
    """
    API для записи участника на категорию.
    """

    def post(self, request):
        pass
