from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view()
def health_check(request):
    """
    Эндпоинт для проверки того, что сервер работает.
    """
    return Response(status=200)
