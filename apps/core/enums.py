from django.db.models import IntegerChoices


class TicketType(IntegerChoices):
    """
    Типы билета (обычный, ВИП).
    """

    REGULAR = 1
    VIP = 2


class BustType(IntegerChoices):
    """
    Тип бюста.
    """

    NATURAL = 1, "Natural"
    IMPLANTS = 2, "Implants"


class OrderStatusType(IntegerChoices):
    """
    Статусы заказа.
    """

    NOT_CONFIRMED = 1, "Not confirmed"
    AWAITING_PAYMENT = 2, "Awaiting payment"
    IN_THE_WORK = 3, "In the work"
    IS_MADE = 4, "Is made"
    SENT = 5, "Sent"
