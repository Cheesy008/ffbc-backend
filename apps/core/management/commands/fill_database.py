from django.core.management.base import BaseCommand

from tests.test_users.factories import UserFactory


class Command(BaseCommand):
    help = "Команда для заполнения бд тестовыми данными"

    def handle(self, *args, **options):
        UserFactory.create_batch(20)
