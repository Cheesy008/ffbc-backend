from rest_framework.throttling import AnonRateThrottle


class SendEmailRateThrottle(AnonRateThrottle):
    scope = "send_email"
