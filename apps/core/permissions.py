from rest_framework.permissions import BasePermission


class IsVerified(BasePermission):
    message = "User is not verified"

    def has_permission(self, request, view):
        return request.user.is_verified


class IsOwner(BasePermission):
    message = "User has no permission to edit this object"

    def has_object_permission(self, request, view, obj):
        return request.user == obj.user


class IsCartOwner(BasePermission):
    message = "User has no permission to edit this cart"

    def has_object_permission(self, request, view, obj):
        return request.user == obj.cart.user
