import shutil
import tempfile

import pytest

MEDIA_ROOT = tempfile.mkdtemp()


@pytest.fixture(autouse=True)
def redefine_settings(settings):
    settings.TESTING = True
    settings.MEDIA_ROOT = MEDIA_ROOT
    settings.REST_FRAMEWORK = {
        "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.AllowAny"],
        "DEFAULT_AUTHENTICATION_CLASSES": [
            "rest_framework_simplejwt.authentication.JWTAuthentication",
        ],
        "DEFAULT_THROTTLE_RATES": {
            "send_email": "100/min",
        },
        "DEFAULT_PAGINATION_CLASS": "apps.core.pagination.CustomPageNumberPagination",
        "PAGE_SIZE": 20,
        "EXCEPTION_HANDLER": "apps.core.utils.custom_exception_handler",
    }


@pytest.fixture(scope="session", autouse=True)
def cleanup():
    """
    Очистка медиа директории после того как все тесты выполнились.
    """
    yield None
    shutil.rmtree(MEDIA_ROOT, ignore_errors=True)
