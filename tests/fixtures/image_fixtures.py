from io import BytesIO

import pytest
from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile


@pytest.fixture
def get_image_file():
    def inner():
        f = BytesIO()
        image = Image.new(mode="RGB", size=(100, 100))
        image.save(f, "png")
        f.seek(0)
        return SimpleUploadedFile("test.jpg", f.read())

    return inner
