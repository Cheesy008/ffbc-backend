import pytest
from rest_framework.test import APIClient


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def test_user(db, user_factory):
    return user_factory.create()


@pytest.fixture
def api_client_with_credentials(db, test_user):
    client = APIClient()
    client.force_authenticate(user=test_user)
    return client


@pytest.fixture
def custom_api_client_with_credentials(db):
    """
    api_client, в который можно передать
    кастомного пользователя.
    """

    def api_client(user):
        client = APIClient()
        client.force_authenticate(user=user)
        return client

    return api_client
