import factory

from apps.orders.models import Order, OrderItem, OrderMeasure, OrderContactService


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order


class OrderContactServiceFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    contact_service = factory.SubFactory("tests.test_users.factories.ContactServiceFactory")

    class Meta:
        model = OrderContactService


class OrderItemFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    product = factory.SubFactory("tests.test_products.factories.ProductFactory")

    class Meta:
        model = OrderItem

    @factory.post_generation
    def product_feature_fields(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for feature in extracted:
                self.product_feature_fields.add(feature)


class OrderMeasureFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    measure_field = factory.SubFactory("tests.test_products.factories.MeasureFieldFactory")

    class Meta:
        model = OrderMeasure
