from django.urls import reverse

from apps.core.enums import BustType, OrderStatusType
from apps.orders.models import (
    Order,
    OrderItem,
    OrderAdditionalPhoto,
)


def test_create_order(api_client):
    payload = {
        "first_name": "string",
        "last_name": "string",
        "email": "user@example.com",
        "street": "string",
        "city": "string",
        "country": "string",
        "state": "string",
        "zip_code": "12345",
        "phone_number": "+77074759672",
        "instagram_url": "https://asd.com",
        "birth_date": "2022-02-06",
        "height": 170,
        "competition_description": "string",
        "bust_type": BustType.NATURAL,
        "contacts": "string",
        "weight": 70,
    }

    url = reverse("orders-list")
    res = api_client.post(url, data=payload, format="json")
    assert res.status_code == 201

    assert Order.objects.count() == 1
    order = Order.objects.first()
    assert order.total_price == 0
    assert order.order_status == OrderStatusType.NOT_CONFIRMED


def test_create_order_items(
    api_client,
    product_factory,
    product_feature_field_factory,
    order_factory,
):
    order = order_factory()
    product1 = product_factory(base_price=100)
    product2 = product_factory(base_price=200)

    payload = [
        {
            "order": order.id,
            "product": product1.id,
            "feature_fields": [
                product_feature_field_factory(feature_price=25).id for _ in range(3)
            ],
        },
        {
            "order": order.id,
            "product": product2.id,
            "feature_fields": [
                product_feature_field_factory(feature_price=30).id for _ in range(2)
            ],
        },
    ]

    url = reverse("order_items-list")
    res = api_client.post(url, data=payload, format="json")

    assert res.status_code == 201
    assert Order.objects.count() == 1
    assert OrderItem.objects.count() == 2
    order_item1 = OrderItem.objects.get(product=product1)
    order_item2 = OrderItem.objects.get(product=product2)
    assert order_item1.price == 175
    assert order_item2.price == 260


def test_create_order_photos(
    api_client,
    get_image_file,
    order_factory,
):
    order = order_factory()
    payload = {"order": order.id, "photo": [get_image_file(), get_image_file()]}

    url = reverse("order_photos-list")
    res = api_client.post(url, data=payload)

    assert res.status_code == 201
    assert OrderAdditionalPhoto.objects.count() == 2


def test_list_orders(
    api_client,
    product_factory,
    measure_field_factory,
    order_factory,
    order_item_factory,
    order_measure_factory,
    order_contact_service_factory,
    contact_service_factory,
):
    product = product_factory()
    measure_fields = [measure_field_factory() for _ in range(3)]
    contact_services = [contact_service_factory() for _ in range(3)]

    order = order_factory()
    [order_item_factory(order=order, product=product) for _ in range(3)]
    [
        order_measure_factory(order=order, measure_field=measure_field)
        for measure_field in measure_fields
    ]
    [
        order_contact_service_factory(order=order, contact_service=contact_service)
        for contact_service in contact_services
    ]

    url = reverse("orders-list")
    res = api_client.get(url)
    assert res.status_code == 200
    res = res.json()["results"][0]
    assert len(res["order_items"]) == 3
    assert len(res["measures"]) == 3
    assert len(res["contact_services"]) == 3
