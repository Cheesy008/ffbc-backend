import random

import factory
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_save
from faker import Factory as FakerFactory

from apps.users.models import ContactService

User = get_user_model()

faker = FakerFactory.create()


@factory.django.mute_signals(post_save)
class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    email = factory.Faker("email")
    password = factory.LazyFunction(lambda: make_password("password"))
    phone_number = factory.Sequence(
        lambda n: f"+77074{random.randint(1, 9)}{random.randint(1, 9)}{random.randint(1, 9)}67{random.randint(1, 9)}"
    )
    is_verified = True


class ContactServiceFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(lambda n: "service {}".format(n))

    class Meta:
        model = ContactService
