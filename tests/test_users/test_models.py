from datetime import date

from dateutil.relativedelta import relativedelta
from django.contrib.auth import get_user_model

User = get_user_model()


def test_user_age(user_factory):
    birth_date = date(2000, 10, 2)

    user = user_factory.create()
    user.birth_date = birth_date
    user.save()

    age = relativedelta(date.today(), birth_date).years

    assert user.age == age


def test_user_change_password(user_factory):
    user = user_factory.create()
    user.change_password("new_pass")

    assert user.check_password("new_pass")
