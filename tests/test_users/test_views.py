import pytest
from django.core.exceptions import ValidationError
from rest_framework.reverse import reverse

# region AuthEndpoints
from apps.users.models import UserMeasure


@pytest.mark.parametrize(
    "email, password, password_confirmation, phone_number, status_code",
    [
        ("test1@gmail.com", "password", "password", "+77074759672", 200),
        (
            "existinguser@gmail.com",
            "password",
            "password123",
            "+7707475961",
            400,
        ),  # пользователь с таким емайлом существует
    ],
)
def test_register_endpoint(
    mocker,
    api_client,
    user_factory,
    email,
    password,
    password_confirmation,
    phone_number,
    status_code,
):
    user_factory.create(email="existinguser@gmail.com")

    endpoint = reverse("auth-register")
    payload = {
        "email": email,
        "password": password,
        "password_confirmation": password_confirmation,
        "phone_number": phone_number,
    }
    mocker.patch("apps.users.services.send_email_task")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


def test_verify_user_email_endpoint(mocker, api_client, user_factory):
    user_factory.create(email="existinguser@gmail.com")

    endpoint = reverse("auth-verify-email")
    payload = {"token": "fghjkdfghjkfghjk43fdwedf"}
    mocker.patch("apps.users.serializers.auth.verify_user_email")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == 200


@pytest.mark.parametrize(
    "email, status_code",
    [
        ("existinguser@gmail.com", 200),
        ("unknown@gmail.com", 400),  # пользователя с таким емайлом не существует
    ],
)
def test_request_reset_password_endpoint(
    mocker,
    api_client,
    user_factory,
    email,
    status_code,
):
    endpoint = reverse("auth-request-reset-password")
    payload = {"email": email}
    mocker.patch("apps.users.services.send_email_task")

    user_factory.create(email="existinguser@gmail.com")
    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "uidb64, token, use_side_effect, status_code",
    [
        ("ab", "hgfdgh", False, 200),
        ("ab", "xvbvcbcvb", True, 400),  # Некорректный токен
    ],
)
def test_password_reset_confirm_endpoint(
    mocker, api_client, uidb64, token, use_side_effect, status_code
):
    payload = {"uidb64": uidb64, "token": token}
    endpoint = reverse("auth-password-reset-confirm")

    if use_side_effect:
        mocker.patch(
            "apps.users.serializers.auth.check_reset_password_token",
            side_effect=ValidationError("error"),
        )
    else:
        mocker.patch(
            "apps.users.serializers.auth.check_reset_password_token",
            return_value=payload,
        )

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "password, password_confirmation, uidb64, token, use_side_effect, status_code",
    [
        ("password", "password", "ab", "ffdfgfd", False, 200),
        (
            "password",
            "password123",
            "ab",
            "ffdfgfd",
            False,
            400,
        ),  # Пароли не совпадают
        (
            "password",
            "password123",
            "ab",
            "ffdfgfd23432",
            False,
            400,
        ),  # Некорректный токен
    ],
)
def test_set_new_password_endpoint(
    mocker,
    api_client,
    password,
    password_confirmation,
    uidb64,
    token,
    use_side_effect,
    status_code,
):
    payload = {
        "password": password,
        "password_confirmation": password_confirmation,
        "uidb64": uidb64,
        "token": token,
    }
    endpoint = reverse("auth-set-new-password")

    if use_side_effect:
        mocker.patch(
            "apps.users.serializers.auth.set_new_password",
            side_effect=ValidationError("error"),
        )
    else:
        mocker.patch("apps.users.serializers.auth.set_new_password")

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


@pytest.mark.parametrize(
    "email, password, is_active, is_verified, status_code",
    [
        ("existinguser@gmail.com", "password", True, True, 200),
        (
            "existinguser@gmail.com",
            "incorrectpass",
            True,
            True,
            400,
        ),  # некорректный пароль
        (
            "existinguser@gmail.com",
            "incorrectpass",
            True,
            False,
            400,
        ),  # неверифицированный пользователь
        (
            "existinguser@gmail.com",
            "incorrectpass",
            False,
            True,
            400,
        ),  # неактивный пользователь
        (
            "unknown@gmail.com",
            "password",
            False,
            False,
            400,
        ),  # пользователя с таким емайлом не существует
    ],
)
def test_login_endpoint(
    api_client,
    user_factory,
    email,
    password,
    is_active,
    is_verified,
    status_code,
):
    user_factory.create(
        email="existinguser@gmail.com", is_active=is_active, is_verified=is_verified
    )

    endpoint = reverse("auth-login")
    payload = {"email": email, "password": password}

    response = api_client.post(endpoint, data=payload, format="json")
    assert response.status_code == status_code


# endregion


# region UserEndpoints
def test_current_user_endpoint(api_client_with_credentials):
    endpoint = reverse("user-current_user")

    response = api_client_with_credentials.get(endpoint)
    assert response.status_code == 200


def test_current_user_is_not_verified(custom_api_client_with_credentials, user_factory):
    endpoint = reverse("user-current_user")
    user = user_factory.create(is_verified=False)

    response = custom_api_client_with_credentials(user).get(endpoint)
    assert response.status_code == 403


def test_edit_current_user_endpoint(
    api_client_with_credentials, contact_service_factory, get_image_file
):
    endpoint = reverse("user-edit_current_user")
    payload = {
        "email": "test@gmail.com",
        "first_name": "test",
        "last_name": "testov",
        "phone_number": "+77074759621",
        "birth_date": "2000-10-02",
        "avatar": get_image_file(),
        "document_photos": [get_image_file()],
        "height": 180,
        "weight": 100,
        "street": "adasd",
        "contact_services": [contact_service_factory().id, contact_service_factory().id],
    }
    response = api_client_with_credentials.post(endpoint, data=payload)
    assert response.status_code == 200


# endregion

# region UserMeasure


def test_create_user_measure(api_client_with_credentials, test_user):
    endpoint = reverse("user_measures-list")
    payload = [
        {"user": test_user.id, "measure_field": None, "value": "abc"},
        {"user": test_user.id, "measure_field": None, "value": "abc"},
    ]

    response = api_client_with_credentials.post(endpoint, data=payload, format="json")
    assert response.status_code == 201
    assert UserMeasure.objects.count() == 2


# endregion
