from rest_framework.reverse import reverse


def test_full_login_logout(api_client, user_factory):
    """
    Тестирование аутентификации, получение текущего пользователя
    и выход.
    """
    login_endpoint = reverse("auth-login")
    user_endpoint = reverse("user-current_user")
    logout_endpoint = reverse("auth-logout")

    email = "testuser@gmail.com"
    password = "password"
    user_factory.create(email=email)

    login_response = api_client.post(
        login_endpoint, data={"email": email, "password": password}, format="json"
    )
    assert login_response.status_code == 200
    assert login_response.data != {}

    access_token = login_response.data["tokens"]["access"]
    refresh_token = login_response.data["tokens"]["refresh"]
    api_client.credentials(HTTP_AUTHORIZATION=f"Bearer {access_token}")

    user_response = api_client.get(user_endpoint)
    assert user_response.status_code == 200
    assert user_response.data != {}

    logout_response = api_client.post(
        logout_endpoint, data={"refresh": refresh_token}, format="json"
    )
    assert logout_response.status_code == 204
