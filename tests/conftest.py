from pytest_factoryboy import register

from .test_catalogs.factories import GalleryCategoryFactory, SponsorFactory
from .test_competitions.factories import CompetitionFactory
from .test_orders.factories import (
    OrderFactory,
    OrderItemFactory,
    OrderMeasureFactory,
    OrderContactServiceFactory,
)
from .test_products.factories import (
    ProductTypeFactory,
    ProductFeatureFieldFactory,
    MeasureFieldFactory,
    MeasureCategoryFactory,
    ProductFeatureFactory,
    ProductFactory,
)
from .test_users.factories import UserFactory, ContactServiceFactory

# Users
register(UserFactory)
register(ContactServiceFactory)

# Catalogs
register(GalleryCategoryFactory)
register(SponsorFactory)
register(CompetitionFactory)

# Products
register(ProductTypeFactory)
register(ProductFeatureFieldFactory)
register(MeasureFieldFactory)
register(MeasureCategoryFactory)
register(ProductFeatureFactory)
register(ProductFactory)

# Orders
register(OrderFactory)
register(OrderItemFactory)
register(OrderMeasureFactory)
register(OrderContactServiceFactory)

pytest_plugins = [
    "tests.fixtures.api_fixtures",
    "tests.fixtures.image_fixtures",
    "tests.fixtures.settings_fixtures",
]


def pytest_collection_modifyitems(items):
    for item in items:
        item.add_marker("django_db")
