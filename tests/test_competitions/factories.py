import random
from datetime import datetime
import pytz

import factory
from factory import fuzzy
from faker import Factory as FakerFactory

from apps.competitions.models import CompetitionImage, Competition, CompetitionCategory
from tests.test_catalogs.factories import SponsorFactory


faker = FakerFactory.create()


class CompetitionImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CompetitionImage

    image = factory.django.ImageField()
    competition = None


class CompetitionCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CompetitionCategory

    name = factory.Sequence(lambda x: f"Competition category name {x}")
    price = fuzzy.FuzzyDecimal(300, 400)
    competition = None


class CompetitionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Competition

    name = factory.Sequence(lambda x: f"Competition name {x}")
    date = fuzzy.FuzzyDateTime(datetime(2008, 1, 1, tzinfo=pytz.UTC))
    location = "Some location"
    description = "Some description"
    images = factory.RelatedFactoryList(
        CompetitionImageFactory,
        factory_related_name="competition",
        size=lambda: random.randint(1, 5),
    )
    competition_categories = factory.RelatedFactoryList(
        CompetitionCategoryFactory,
        factory_related_name="competition",
        size=lambda: random.randint(1, 5),
    )
    vip_ticket_price = fuzzy.FuzzyDecimal(100, 200)
    regular_ticket_price = fuzzy.FuzzyDecimal(50, 80)

    @factory.post_generation
    def sponsors(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for sponsor in extracted:
                self.sponsors.add(sponsor)
        else:
            self.sponsors.add(SponsorFactory())
