import json

from rest_framework.reverse import reverse

from apps.competitions.models import Competition

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"


def test_list_competition_endpoint(api_client, competition_factory):
    competition_factory.create_batch(5)

    endpoint = reverse("competition-list")
    response = api_client.get(endpoint)
    parsed_json = json.loads(response.content)

    assert response.status_code == 200
    assert len(parsed_json["results"]) == 5


def test_retrieve_competition_endpoint(api_client, competition_factory):
    competition = competition_factory.create()

    endpoint = reverse("competition-detail", kwargs={"pk": competition.id})
    response = api_client.get(endpoint)

    assert response.status_code == 200
    assert Competition.objects.count() == 1
