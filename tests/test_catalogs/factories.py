import random

import factory
from factory import fuzzy
from faker import Factory as FakerFactory

from apps.catalogs.models import GalleryCategory, GalleryImage, Sponsor

faker = FakerFactory.create()


class GalleryCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GalleryCategory

    name = fuzzy.FuzzyText(length=10)
    images = factory.RelatedFactoryList(
        "tests.test_catalogs.factories.GalleryImageFactory",
        factory_related_name="gallery_category",
        size=lambda: random.randint(1, 5),
    )


class GalleryImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GalleryImage

    image = factory.django.ImageField()
    gallery_category = None


class SponsorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Sponsor

    name = fuzzy.FuzzyText(length=10)
    logo = factory.django.ImageField()
    description = fuzzy.FuzzyText(length=20)
