import json

from rest_framework.reverse import reverse

from apps.catalogs.models import GalleryCategory, Sponsor


class TestGalleryEndpoints:
    def test_list_endpoint(self, api_client, gallery_category_factory):
        gallery_category_factory.create_batch(size=5)

        endpoint = reverse("gallery-list")
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert len(parsed_json["results"]) == 5

    def test_retrieve_endpoint(self, api_client, gallery_category_factory):
        gallery = gallery_category_factory.create()

        endpoint = reverse("gallery-detail", kwargs={"pk": gallery.id})
        response = api_client.get(endpoint)

        assert response.status_code == 200
        assert GalleryCategory.objects.count() == 1


class TestSponsorEndpoints:
    def test_list_endpoint(self, api_client, sponsor_factory):
        sponsor_factory.create_batch(size=5)

        endpoint = reverse("sponsor-list")
        response = api_client.get(endpoint)
        parsed_json = json.loads(response.content)

        assert response.status_code == 200
        assert len(parsed_json["results"]) == 5

    def test_retrieve_endpoint(self, api_client, sponsor_factory):
        sponsor = sponsor_factory.create()

        endpoint = reverse("sponsor-detail", kwargs={"pk": sponsor.id})
        response = api_client.get(endpoint)

        assert response.status_code == 200
        assert Sponsor.objects.count() == 1
