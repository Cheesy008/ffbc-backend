import factory

from apps.products.models import (
    Product,
    ProductType,
    ProductFeature,
    ProductFeatureField,
    MeasureField,
    MeasureCategory,
)


class MeasureCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MeasureCategory


class MeasureFieldFactory(factory.django.DjangoModelFactory):
    measure_category = factory.SubFactory(MeasureCategoryFactory)

    class Meta:
        model = MeasureField


class ProductFeatureFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductFeature


class ProductFeatureFieldFactory(factory.django.DjangoModelFactory):
    feature = factory.SubFactory(ProductFeatureFactory)

    class Meta:
        model = ProductFeatureField


class ProductTypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ProductType

    @factory.post_generation
    def measure_categories(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for category in extracted:
                self.measure_categories.add(category)


class ProductFactory(factory.django.DjangoModelFactory):
    product_type = factory.SubFactory(ProductTypeFactory)

    class Meta:
        model = Product
